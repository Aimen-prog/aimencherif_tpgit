#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    This library checks whether a dna sequence is valid. 
    If not, it asks the user to input a sequence once again. 
"""

__author__ = 'Aimen CHERIF'


def is_valid(adn) :
    adn=adn.upper()  #Transforms all the sequence to uppercase
    adn=adn.replace(' ','') #removes spaces from the input
    valide=True
    for base in adn:
        if not base in 'ATCG':
            return False
            valide=False
            break
    if valide==True :
        return True


def get_valid_adn(prompt='chaîne : ') :

    ADN=input(prompt)
    ADN=ADN.replace(' ','') #removing spaces to have an appropriate result
    while is_valid(ADN)==False :
        ADN=input("Entrez une séquence ADN valide : ")
    print (prompt,ADN)


