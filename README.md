README
======

# TP Programmation Python
Ce [TP](https://github.com/olivierChabrol/tpGit.git) a été réalisé dans le cadre de l'UE INLO.



**Mode d'emploi**
<br>
Ce programme vérifie si une chaine ADN est valide ou invalide.  



**Code**
<br>
La librairie *adn.py*

<ol type="I"> 
<li> Une premiére fonction is_valid(adn) prend en entrée une chaîne de caractère (​ adn​ ) et renvoie True​ si elle n’est composée que de ‘​ t ​ ’, ‘​ a ​ ’, ‘​ g ​ ’ ou ‘​ c ​ ’ ; ​ False​ sinon.</li>

<li> Une deuxiéme fonction get_valid_adn() qui demande à l’utilisateur d’entrer une chaîne ADN tant que celle-ci n’est pas valide. </li>
</ol>



